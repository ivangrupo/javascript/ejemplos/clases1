/*JS DOCUMENT */

/** CREAR CLASES **/

// Con funciones declaradas

// Se puede crear con:  class --> Ahora mismo sólo con (ES6)

/**

1º Se crea la clase
2º Se instancia la clase
3º Ya tienes el objeto

clase ----> instanciar ----> objeto

**/

/** CREAR UNA CLASE **/

// Esto es una clase, poner la primera letra en mayúscula siempre (simula una clase, pero es una funcion prototipo), una clase es class SeleccionFutbol{};
var Forma = function(padre) {

    this.area=0;

    this.setArea = function(valor){
        this.area=valor || 0;
    }

    this.getArea = function(){
        return this.area;
    }

    this.forma=function(valores){

        this.setArea(valores.area);

    }

    this.forma(padre);
    
}

var Cuadrado = function(hijo) {

    this.lado=0;

    this.setLado = function(valor){
        this.lado=valor || 0;
    }

    this.getLado = function(){
        return this.lado;
    }

    this.cuadrado=function(valores){

        this.setLado(valores.lado);
        // this.forma(valores);
        Forma.call(this,valores);

    }

    this.cuadrado(hijo);
    
}


// Cuadrado.prototype=new Forma({});    Al usar this.call(this,argumento) en el constructor del hijo, ya no hace falta llamar al padre


/** Instanciar clase **/

objeto1=new Forma({

    area:100,

});

objeto2=new Cuadrado({

    lado:10,
    area:200,

});


/** Utilizar objeto **/

console.log(objeto1);
console.log(objeto2);
console.log(objeto2.getArea());



// (ES6)

// class Forma{
//     construct(arg){
//         this.nombre=arg.nombre
//     }
//     set nombre(){
//         this.nombre=arg;
//     }
//     get nombre(){
//         return this.nombre;
//     }
// }