/*JS DOCUMENT */

/** CREAR CLASES **/

// Con funciones declaradas

// Se puede crear con:  class --> Ahora mismo sólo con (ES6)	

/**

1º Se crea la clase
2º Se instancia la clase
3º Ya tienes el objeto

clase ----> instanciar ----> objeto

**/

/** CREAR UNA CLASE **/

// Esto es una clase, poner la primera letra en mayúscula siempre
var Persona = function(padre) {

	var sup = this; // Variable privada especial (tiene toda la clase metida) Te permite acceder a métodos públicos desde un privado
		var nombre;
    	var apellidos;
    	var nombreCompleto;
    	var fechaNacimiento;
    	var direccion;
    	var edad;


    // Método público, accesible desde fuera, puede acceder a públicos y privados

        this.setNombre = function(valor) {
            nombre = valor || "";
        }

        this.getNombre = function() {
            return nombre;
        }

        this.setApellidos = function(valor) {
            apellidos = valor || "";
        }

        this.getApellidos = function() {
            return apellidos;
        }

        this.setNombreCompleto = function(valor) {

            nombreCompleto = valor;
        }

        this.getNombreCompleto = function() {

        	nombreCompleto = sup.getNombre() + " " + sup.getApellidos();

            return nombreCompleto;
        }

        this.setFechaNacimiento = function(valor) {
            fechaNacimiento = valor || "1/1/1990";
            // fechaNacimiento={
            // 	dia:__,
            // 	mes:__,
            // 	año:__;
            // }
        }

        this.getFechaNacimiento = function() {
            return fechaNacimiento;
        }

        this.setDireccion = function(valor) {
            direccion = valor || "";;
        }

        this.getDireccion = function() {
            return direccion;
        }

        this.setEdad = function(valor) {
            edad = valor || 0;
        }

        this.getEdad = function() {

        	var hoy=new Date();
        	var vector=sup.getFechaNacimiento().split("/");
        	var fn= new Date(vector[2], vector[1], vector[0]);

            return (hoy.getFullYear()-fn.getFullYear());
        }

    // Función pública constructora de la clase persona, esta función en otros lenguajes se ejecuta sola.
    this.persona=function(valores){

    	this.setNombre(valores.nombre);
        this.setApellidos(valores.apellidos);
        this.setNombreCompleto(valores.nombreCompleto);
        this.setFechaNacimiento(valores.fechaNacimiento);
        this.setDireccion(valores.direccion);
        this.setEdad(valores.edad);

    }

    this.persona(padre);

    // Función constructora de la clase persona. Así sería privada.
    // function persona(){
    // 	this.setNombre(valores.nombre);
    // 	-
    // 	-
    // }
    // persona();

}


Trabajador.prototype=new Persona({});	//	Trabajador adopta todos los métodos y propiedades de Persona

// var objetoPadre=new Persona();
// var objetoHijo=new Trabajador();

// console.log(objetoPadre.nombrePadre);
// console.log(objetoHijo.nombrePadre);
// console.log(objetoHijo.nombreHijo);



/** Instanciar clase **/

var jose = new Persona({
    nombre: "José",
    apellidos: "Vázquez Rodríguez",
    fechaNacimiento: "23/12/1985",
    direccion: "Calle Blabla 7",
    edad: "25",
});


/** Utilizar objeto **/

console.log(jose);
console.log(jose.getNombreCompleto());
console.log(jose.getEdad());


var Trabajador = function(hijo){

	var sup=this;
	var sueldo;
    var hijos;
    var empresa;
    var fechaEntrada;
   	var moneda="€";

	this.setSueldo = function(valor) {
		// if (typeof(sueldo)=="undefined") {		//	Eso es una regla de negocio y siempre van en los setters
  //       	sueldo = valor || 0;
  //   	}else if(typeof(valor)!="undefined"){
  //   		sueldo = valor;
  //   	}
  //   	
  		sueldo = valor || 0;
    }

    this.getSueldo = function() {
        return sueldo;
    }

    this.setHijos = function(valor) {
        hijos = valor || 0;
    }

    this.getHijos = function() {
        return hijos;
    }

    this.setEmpresa = function(valor) {
        empresa = valor || "";
    }

    this.getEmpresa = function() {
        return empresa;
    }

    this.setFechaEntrada = function(valor) {
        fechaEntrada = valor || "1/1/2000";
    }

    this.getFechaEntrada = function() {
        return fechaEntrada;
    }


    function concatenar(){

		return parseFloat(sup.sueldo) + parseFloat(sup.moneda);

	}


	this.trabajador=function(valores){
		this.persona(valores);
    	this.setSueldo(valores.sueldo);
        this.setHijos(valores.hijos);
        this.setEmpresa(valores.empresa);
        this.setFechaEntrada(valores.fechaEntrada);

    }

    this.trabajador(hijo);

}


/** Instanciar clase **/

var silvia = new Trabajador({
    sueldo: 1000,
    hijos: 5,
    empresa:"Alpe",
    fechaEntrada:"1/1/2015",
    nombre:"Silvia",
});


/** Utilizar objeto **/

console.log(silvia.getSueldo());
console.log(silvia.getEmpresa());
console.log(silvia.getNombre());














// adjudicar set
// devolver get

// Hacer todo clase persona

// getNombreCompleto
// getFechaCompleto

// devolver



/** ELEMENTOS DE UNA CLASE **/

/**

Los elementos son: Públicos, privados

Los elementos tienen: Propiedades y métodos.

Crear objeto sólo puede acceder a los elementos públicos

/** MÉTODOS

getter, setter(da valor a una propiedad) y constructores (siempre son públicos)

+ ancho		// Con los públicos se utiliza +
- unidad	// Con los privados se utiliza -

+crearCaja();
-concatenar();

+setAncho(valor) --> el valor sería el ancho
+getAncho()

*/