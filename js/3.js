/*JS DOCUMENT */

/** CREAR LA SUPERCLASE **/

// Superclase seleccionFutbol (simula una clase, pero es una funcion prototipo), una clase es class SeleccionFutbol{};
var SeleccionFutbol = function(padre) {

    this.id=0;
    this.nombre="";
    this.apellidos="";
    this.edad=0;
    
    this.concentrarse = function(){
        return "En la concentración";
    };

    this.viajar = function(){
        return "Viajando con el equipo";
    };


    this.setId = function(valor){
        this.id=valor || 0;
    }

    this.getId = function(){
        return this.id;
    }

    this.setNombre = function(valor){
        this.nombre=valor || "";
    }

    this.getNombre = function(){
        return this.nombre;
    }

    this.setApellidos = function(valor){
        this.apellidos=valor || "";
    }

    this.getApellidos = function(){
        return this.apellidos;
    }

    this.setEdad = function(valor){
        this.edad=valor || 0;
    }

    this.getEdad = function(){

        var hoy=new Date();
            var vector=this.edad.split("/");
            var fn= new Date(vector[2], vector[1], vector[0]);

            return (hoy.getFullYear()-fn.getFullYear());
    }


    this.seleccionFutbol=function(valores){

        this.setId(valores.id);
        this.setNombre(valores.nombre);
        this.setApellidos(valores.apellidos);
        this.setEdad(valores.edad);
        this.concentrarse();
        this.viajar();

    }

    this.seleccionFutbol(padre);
    
}

/** Instanciar clase **/

var seleccionFutbol1=new SeleccionFutbol({

    id: 1,
    nombre: "Real Madrid",
    apellidos: "C. F.",
    edad: "6/3/1902",

});


/** Utilizar objeto **/

console.log("- seleccionFutbol1");
console.log(seleccionFutbol1);
console.log(seleccionFutbol1.getId());
console.log(seleccionFutbol1.getNombre() + " " + seleccionFutbol1.getApellidos());
console.log(seleccionFutbol1.getEdad());
console.log(seleccionFutbol1.concentrarse());
console.log(seleccionFutbol1.viajar());
console.log("");


//  Clase Futbolista
var Futbolista = function(hijo) {

    this.dorsal=0;
    this.demarcacion="";
    
    this.jugarPartido = function(){
        return "Jugando el partido";
    };

    this.entrenar = function(){
        return "Entrenando con el equipo";
    };


    this.setDorsal = function(valor){
        this.dorsal=valor || 0;
    }

    this.getDorsal = function(){
        return this.dorsal;
    }

    this.setDemarcacion = function(valor){
        this.demarcacion=valor || "";
    }

    this.getDemarcacion = function(){
        return this.demarcacion;
    }


    this.futbolista=function(valores){

        this.setDorsal(valores.dorsal);
        this.setDemarcacion(valores.demarcacion);
        this.jugarPartido();
        this.entrenar();

        SeleccionFutbol.call(this,valores);

    }

    // if (typeof(hijo)=="undefined") {
    //     this.futbolista({});             // Si no hacemos un JSON al instanciar la clase hija
    // } else {
    //     this.futbolista(hijo);
    // }

    this.futbolista(hijo);
    
}


/** Instanciar clase **/

var futbolista1=new Futbolista({

    id: 106,
    nombre: "Nacho",
    apellidos: "Fernández",
    edad: "6/3/1902",
    dorsal: 6,
    demarcacion: "DEF",

});


/** Utilizar objeto **/

console.log("- futbolista1");
console.log(futbolista1);
console.log(futbolista1.getApellidos());
// console.log(futbolista1.getDorsal());
console.log("");


//  Clase Entrenador
var Entrenador = function(hijo) {

    this.idFederacion=0;

    this.setIdFederacion = function(valor){
        this.idFederacion=valor || 0;
    }

    this.getIdFederacion = function(){
        return this.idFederacion;
    }

    this.dirigirPartido = function(){
        return "Dirigiendo el partido";
    };

    this.dirigirEntrenamiento = function(){
        return "Dirigiendo el entrenamiento";
    };


    this.entrenador=function(valores){

        this.setIdFederacion(valores.idFederacion);
        this.dirigirPartido();
        this.dirigirEntrenamiento();

        SeleccionFutbol.call(this,valores);

    }

    this.entrenador(hijo);
    
}


/** Instanciar clase **/

var entrenador1=new Entrenador({

    id: 131,
    nombre: "Julen",
    apellidos: "Lopetegui",
    edad: "6/3/1962",
    idFederacion:72063911,

});


/** Utilizar objeto **/

console.log("- entrenador1");
console.log(entrenador1);
console.log(entrenador1.getId());
console.log(entrenador1.getNombre());
console.log("");


//  Clase Masajista
var Masajista = function(hijo) {

    this.idFederacion=0;
    this.aniosExperiencia=0;
    this.darMasaje=null;

    this.setTitulacion = function(valor){
        this.titulacion=valor || "";
    }

    this.getTitulacion = function(){
        return this.titulacion;
    }

    this.setAniosExperiencia = function(valor){
        this.aniosExperiencia=valor || 0;
    };

    this.getAniosExperiencia = function(){
        return this.aniosExperiencia;
    }

    this.darMasaje = function(){
       return "Dando un masaje";
    };


    this.masajista=function(valores){

        this.setTitulacion(valores.titulacion);
        this.setAniosExperiencia(valores.aniosExperiencia);
        this.darMasaje();

        SeleccionFutbol.call(this,valores);

    }

    this.masajista(hijo);
    
}


/** Instanciar clase **/

var masajista1=new Masajista({
    id: 141,
    nombre: "Nacho",
    apellidos: "Fernández",
    edad: "6/3/1902",
    titulacion:"Fisioterapeuta Deportivo",
    aniosExperiencia: "6/3/1902"

});


/** Utilizar objeto **/

console.log("- masajista1");
console.log(masajista1);
console.log(masajista1.getTitulacion());
console.log(masajista1.darMasaje());