/*JS DOCUMENT */


/** CREAR LA SUPERCLASE **/

// Superclase Persona (simula una clase, pero es una funcion prototipo), una clase es class SeleccionFutbol{};
var Persona = function(padre) {

    this.nombre="";
    this.direccion="";
    this.correoElectronico="";
    

    this.crear = function(){
        return "Crear";
    };

    this.borrar = function(){
        return "Borrar";
    };

    this.enviarMensaje = function(){
        return "Enviar mensaje";
    };


    this.setNombre = function(valor){
        this.nombre=valor || "";
    }

    this.getNombre = function(){
        return this.nombre;
    }

    this.setDireccion = function(valor){
        this.direccion=valor || "";
    }

    this.getDireccion = function(){
        return this.direccion;
    }

    this.setCorreoElectronico = function(valor){
        this.correoElectronico=valor || "";
    }

    this.getCorreoElectronico = function(){
        return this.correoElectronico;
    }


    this.persona=function(valores){

        this.setNombre(valores.nombre);
        this.setDireccion(valores.direccion);
        this.setCorreoElectronico(valores.correoElectronico);
        this.crear();
        this.borrar();
        this.enviarMensaje();

    }

    this.persona(padre);
    
}


/** Instanciar clase **/

var persona1=new Persona({

    nombre:"Iván",

});


/** Utilizar objeto **/

console.log("- persona1");
console.log(persona1);
console.log(persona1.getNombre());
console.log("");



//  Clase Cliente
var Cliente = function(hijo) {

    this.numeroCliente=0;
    this.fechaAlta="";
    

    this.verFechaAlta = function(){
        console.log(this.getFechaAlta());
    };


    this.setNumeroCliente = function(valor){
        this.numeroCliente=valor || 0;
    }

    this.getNumeroCliente = function(){
        return this.numeroCliente;
    }

    this.setFechaAlta = function(valor){
        this.FechaAlta=valor || "";
    }

    this.getFechaAlta = function(){
        return this.fechaAlta;
    }


    this.cliente=function(valores){

        this.setNumeroCliente(valores.numeroCliente);
        this.setFechaAlta(valores.fechaAlta);
        this.verFechaAlta();

        this.persona(valores);

    }

    this.cliente(hijo);
    
}

Cliente.prototype=new Persona({});


/** Instanciar clase **/

var cliente1=new Cliente({

    correoElectronico:"abc@mail.com",

});


/** Utilizar objeto **/

// console.log(cliente1.__proto__.correoElectronico);  // En caso de haber sobreescritura y haber utilizado prototype
console.log("- cliente1");
console.log(cliente1);
console.log(cliente1.getCorreoElectronico());
console.log("");



//  Clase Usuario
var Usuario = function(hijo) {

    this.codigoUsuario=0;
    this.autorizar=null;
    

    this.crear = function(){
        return "Creado";
    };


    this.setCodigoUsuario = function(valor){
        this.codigoUsuario=valor || 0;
    }

    this.getCodigoUsuario = function(){
        return this.codigoUsuario;
    }

    this.autorizar = function(){
        return "Autorizado";
    };


    this.usuario=function(valores){

        this.setCodigoUsuario(valores.codigoUsuario);
        this.autorizar();
        this.crear();

        this.persona(valores);

    }

    this.usuario(hijo);
    
}

Usuario.prototype=new Persona({});

/** Instanciar clase **/

var usuario1=new Usuario({
    nombre:"pepito",
    codigoUsuario:7,

});


/** Utilizar objeto **/

console.log("- usuario1");
console.log(usuario1);
console.log(usuario1.getCodigoUsuario());